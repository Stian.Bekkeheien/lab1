package rockPaperScissors;
import java.util.Random;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import javax.print.event.PrintEvent;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    int number = 1;
    String [] rpsChoices = {"rock", "paper", "sciccors"};
    Boolean continuePlaying = true;
    
    public void run() {
        //Generates a random choice from rpsChoices:
        
        

       
        while (true){ 
            Random random = new Random();
            int randomnumber = random.nextInt(rpsChoices.length);
            String data_answer = rpsChoices[randomnumber];
            System.out.println("Let's play round " + roundCounter + "!");
            //takes an input from player, where the player decides what to play.
        
            System.out.println("Your choice (Rock/Paper/Scissors)?");
            String players_answer = sc.nextLine();
            players_answer.toLowerCase();
            String whowon = " ";

            if (players_answer.equals(data_answer)){
                whowon = "It's a tie!";
                }

            else if (players_answer.equals("rock")){
                if (data_answer.equals("sciccors")){
                    whowon = "Computer wins!";computerScore += 1;}
                    
                else if(data_answer.equals("paper")){
                    whowon = "Human won!";humanScore += 1;}
                }
            else if (players_answer.equals("scissors")){
                if (data_answer.equals("paper")){
                    whowon = "Human won!";humanScore += 1;}
                else if (data_answer.equals("rock")){
                    whowon = "Computer won!";computerScore += 1;}
                }
            else if (players_answer.equals("paper")){
                if (data_answer.equals("rock")){
                    whowon = "Human won!";humanScore += 1;}
                else if (data_answer.equals("scissors")){
                    whowon = "Computer won!";computerScore += 1;}
                }
            else {System.out.println("I do not understand " + players_answer + ", could you try again?"); roundCounter -= 1;}
            roundCounter += 1;
            System.out.println("Human chose " + players_answer + ", computer chose " + data_answer + ". " + whowon);
            System.out.println("Score: human " + humanScore + ", computer " + computerScore);
            System.out.println("Do you wish to continue playing? (y/n)?");
            String ynanswer = sc.nextLine();
                if ((ynanswer.toLowerCase()).equals("y")){continue;}
                else {System.out.println("Bye bye :)");break;}
            
            }
        }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
